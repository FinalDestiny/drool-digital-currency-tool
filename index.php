<?php
$currencies = array();
$url = "https://bitpay.com/api/rates";
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
foreach($data as $currency) {
	$currencies[] = array('name' => $currency['name'], 'code' => strtolower($currency['code']), 'rate' => $currency['rate'] );
}
$currencies2 = array();
$currencies2[] = array('name' => 'BitCoin', 'code' => 'btc');
$currencies2[] = array('name' => 'LiteCoin', 'code' => 'ltc');
$currencies2[] = array('name' => 'NovaCoin', 'code' => 'nvc');
$currencies2[] = array('name' => 'NameCoin', 'code' => 'nmc');
$currencies2[] = array('name' => 'PeerCoin', 'code' => 'ppc');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Drool</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/jquery.jqplot.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="preload">
        <img src="images/Preloader_6.gif" />
    </div>

    <div class="container">
        <div class="header">
            <ul class="nav nav-pills pull-right">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <h3 class="text-muted">Project name</h3>
        </div>
         <div class="top_heading">
            <h1>Vizualize and Compare Virtual Currencies</h1>
        </div>
        <div class="conversion_block">
            <div class="left">
            	<div class="left_inner">
			        <span class="amount">Amount:</span>
			        <input type="text" class="number1" value="1" data-autosize-input='{ "space": 10 }' />
			        <select class="currency1">
			            <?php
			            foreach($currencies2 as $currency) {
			                echo '<option value="' . $currency['code'] . '">' . $currency['name'] . '</option>';
			            }
			            ?>
			        </select>
			    </div>
	        </div>

	        <div class="middle">
	            <img src="http://teothemes.com/tw/images/equal.png" alt="equal sign" />
	            <img src="http://teothemes.com/tw/images/exchange.png" class="exchange" alt="exchange sign" />
            </div>

            <div class="right">
            	<div class="right_inner">
	            	<span class="amount">Result:</span>
		            <input type="text" value="" class="number2" data-autosize-input='{ "space": 10 }' /> 
		            <select class="currency2">
		                <?php
		                $i = 1;
		                foreach($currencies as $currency) {
		                	echo '<option value="' . $currency['code'] . '">' . $currency['name'] . '(' . strtoupper($currency['code']) . ')</option>';
		                }
		                ?>
		            </select>
		        </div>
	        </div>
        </div>
        
        <div style="clear: both"></div>
        <div class="separator"></div>

        <div class="heading">
        	<h1>BitCoin recent values</h1>
        </div>

        <div id="chart_btc" style="height:300px;"></div>

        <div class="separator"></div>

        <div class="heading">
            <h1>LiteCoin recent values</h1>
        </div>

        <div id="chart_ltc" style="height:300px;"></div>

        <div class="separator"></div>

         <div class="heading">
            <h1>PeerCoin recent values</h1>
        </div>

        <div id="chart_ppc" style="height:300px;"></div>

        <div class="separator"></div>

        <div class="heading">
            <h1>1 BitCoin Worth</h1>
        </div>

        <div id="chart_btc_comparison" style="height:500px;"></div>


        <div class="footer">
            <p>&copy; Company 2014</p>
        </div>

        
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="js/jquery.autosize.input.js"></script>
    <script type="text/javascript" src="js/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" src="js/highcharts-3d.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
    $(function () {
            <?php 
                $connect = mysqli_connect('localhost', 'teo_cloud', 'f,%..QdNn32&', 'teo_cloud');

                //bitcoins
                $query = mysqli_query($connect, "SELECT * FROM bitcoins LIMIT 100");
                $btc_vals = array();
                while($result = mysqli_fetch_array($query) ) {
                    $btc_vals[] = $result['value'];
                }

                //bitcoins
                $query = mysqli_query($connect, "SELECT * FROM litecoins LIMIT 100");
                $ltc_vals = array();
                while($result = mysqli_fetch_array($query) ) {
                    $ltc_vals[] = $result['value'];
                }

                //peercoins
                $query = mysqli_query($connect, "SELECT * FROM peercoins LIMIT 100");
                $ppc_vals = array();
                while($result = mysqli_fetch_array($query) ) {
                    $ppc_vals[] = $result['value'];
                }
                ?>
            $('#chart_btc').highcharts({
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Source: btc-e.com',
                    x: -20
                },
                yAxis: {
                    title: {
                        text: 'Value ($)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '$'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'BitCoin',
                    data: [<?php echo implode(',', $btc_vals);?>]
                }]
            });

            $('#chart_ltc').highcharts({
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Source: btc-e.com',
                    x: -20
                },
                yAxis: {
                    title: {
                        text: 'Value ($)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '$'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'LiteCoin',
                    data: [<?php echo implode(',', $ltc_vals);?>]
                }]
            });

            $('#chart_ppc').highcharts({
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Source: btc-e.com',
                    x: -20
                },
                yAxis: {
                    title: {
                        text: 'Value ($)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '$'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'PeerCoin',
                    data: [<?php echo implode(',', $ppc_vals);?>]
                }]
            });

            <?php
            //preparing the 3d chart data
            $chart3d_vals = array();
            $chart3d_data = array();
            $chart3d_name = array();
            $i = 0;
            foreach($currencies as $currency) {
                if($currency['rate'] < 10000) {
                    $chart3d_vals[] = number_format($currency['rate'], 1, '.', '');
                    $chart3d_data[] = "'" . strtoupper($currency['code']) . "'";
                    $chart3d_name[] = "'" . strtoupper($currency['name']) . "'";
                }
                $i++;
                //we get just 15 results
                if($i >= 11) 
                    break;
            }

            ?>

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'chart_btc_comparison',
                    type: 'column',
                    margin: 75,
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
                },
                title: {
                    text: '3D Chart'
                },
                subtitle: {
                    text: '1 Bitcoin value compared to real currencies'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                categories: ['Currency']
                },
                series: [
                    <?php 
                    for($i = 0; $i<count($chart3d_data); $i++) {
                        echo '{
                            name: ' . $chart3d_name[$i] . ',
                            data: [' . $chart3d_vals[$i] . ']
                        },';
                    }
                    ?>
                ]
            });
        });
    

        </script>
  </body>
</html>
