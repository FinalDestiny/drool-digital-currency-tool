$( document ).ready(function() {
    $('.preload').fadeOut(400);
    $('.conversion_block select').selectBoxIt();
    $(' .number1').on('input', function() {
    	$.get('conversion.php', 
    		  { 
    		  	nr1: $('.number1').val(), 
    		  	nr2: $('.number2').val(), 
    		  	currency1: $('.currency1 option:selected').val(), 
    		  	currency2: $('.currency2 option:selected').val(), 
    		  	modified: 1
    		  },
    		  function(data) {
    		  	$('.number2').val(data).trigger('change');
    		  }
    	);
    });

    $(' .number2').on('input', function() {
    	$.get('conversion.php', 
    		  { 
    		  	nr1: $('.number1').val(), 
    		  	nr2: $('.number2').val(), 
    		  	currency1: $('.currency1 option:selected').val(), 
    		  	currency2: $('.currency2 option:selected').val(), 
    		  	modified: 2
    		  },
    		  function(data) {
    		  	$('.number1').val(data).trigger('change');
    		  }
    	);
    });

    $(' .currency1').on('change', function() {
    	$.get('conversion.php', 
    		  { 
    		  	nr1: $('.number1').val(), 
    		  	nr2: $('.number2').val(), 
    		  	currency1: $('.currency1 option:selected').val(), 
    		  	currency2: $('.currency2 option:selected').val(), 
    		  	modified: 1
    		  },
    		  function(data) {
    		  	$('.number2').val(data).trigger('change');
    		  }
    	);
    });

    $(' .currency2').on('change', function() {
    	$.get('conversion.php', 
    		  { 
    		  	nr1: $('.number1').val(), 
    		  	nr2: $('.number2').val(), 
    		  	currency1: $('.currency1 option:selected').val(), 
    		  	currency2: $('.currency2 option:selected').val(), 
    		  	modified: 1
    		  },
    		  function(data) {
    		  	$('.number2').val(data).trigger('change');
    		  }
    	);
    });

    $('.number1').trigger('input');

});