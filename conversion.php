<?php

$nr1 = (float)$_GET['nr1'];
$nr2 = (float)$_GET['nr2'];
$currency1 = $_GET['currency1'];
$currency2 = $_GET['currency2'];
$modified = $_GET['modified'];

//we are converting currency1 to currency 2.

if($modified == 1) {
	$url = 'http://teothemes.com/tw/api/convert.php?number=' . $nr1 . '&from=' . $currency1 . '&to=' . $currency2;
}
else {
	$url = 'http://teothemes.com/tw/api/convert.php?number=' . $nr2 . '&from=' . $currency2 . '&to=' . $currency1;
}
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
echo number_format($data['rate'], 4, '.', '');