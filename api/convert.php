<?php
$url = "https://bitpay.com/api/rates";

//used for converting a sum from a currency to another
$number = isset($_GET['number']) ? (float)$_GET['number'] : '';
$from = isset($_GET['from']) ? $_GET['from'] : '';
$to = isset($_GET['to']) ? $_GET['to'] : '';

//second case, when he wants to have the latest value for a digital currency
$getcurrency = isset($_GET['currency']) ? $_GET['currency'] : '';
$allowed = array('btc', 'ltc', 'nvc', 'nmc', 'ppc');


if( ($number == '' || $from == '' || $to == '') && ($getcurrency == '' || !in_array($getcurrency, $allowed) ) ) {
	die("Invalid format");
}

$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$bitcoin = array(); //the array that will store all the bitcoin conversion rates

foreach($data as $currency) {
	$bitcoin[strtolower($currency['code'])] = $currency['rate'];
}
$bitcoin['btc'] = '1'; //for bitcoin usage as parameter

//live btc - bitcoin price
$url = 'https://btc-e.com/api/2/btc_usd/ticker';
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$btc_price = $data['ticker']['last']; 
//ltc - litecoin
$url = 'https://btc-e.com/api/2/ltc_usd/ticker';
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$ltc_price = $data['ticker']['last']; 
$bitcoin['ltc'] = $btc_price / $ltc_price;

//nvc - novacoin
$url = 'https://btc-e.com/api/2/nvc_usd/ticker';
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$nvc_price = $data['ticker']['last']; 
$bitcoin['nvc'] = $btc_price / $nvc_price;

//nmc - namecoin
$url = 'https://btc-e.com/api/2/nmc_usd/ticker';
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$nmc_price = $data['ticker']['last']; 
$bitcoin['nmc'] = $btc_price / $nmc_price;

//ppc - peercoin
$url = 'https://btc-e.com/api/2/ppc_usd/ticker';
$json = file_get_contents($url);
$data = json_decode($json, TRUE);
$ppc_price = $data['ticker']['last']; 
$bitcoin['ppc'] = $btc_price / $ppc_price;
if($getcurrency != '') {
	//we return the latest price available
	$result = array();
	switch($getcurrency) {
		case 'btc':
			$result['code'] = 'btc';
			$result['rate'] = $btc_price;
			break;
		case 'ltc':
			$result['code'] = 'ltc';
			$result['rate'] = $ltc_price;
			break;
		case 'nvc':
			$result['code'] = 'nvc';
			$result['rate'] = $nvc_price;
			break;
		case 'nmc':
			$result['code'] = 'nmc';
			$result['rate'] = $nmc_price;
			break;
		case 'ppc':
			$result['code'] = 'ltc';
			$result['rate'] = $ppc_price;
			break;
	}
	echo json_encode($result);
	die();
}

//we do our conversion based on bitcoin values. 
$from = strtolower($from);
$to = strtolower($to);
if(!isset($bitcoin[$from]) || !isset($bitcoin[$to]) ) {
	die("Currency non-existent");
}
$result['code'] = $to;
$result['rate'] = $number * $bitcoin[$to] / $bitcoin[$from]; 
echo json_encode($result);
?>